from django.contrib.auth.models import User
from rest_framework import viewsets, status
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework.decorators import api_view
from buyerseller.models import Category,SubCategory
from buyerseller.serializers import UserSerializer, CatalogueSerializer
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated




# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    #serializer_class = UserSerializer

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def list_categories(request):
    """
    API endpoint that list all active categories and sub categories
    """
    print(request)
    categories_obj = Category.objects.filter(is_active=True)
    subcategory_obj = SubCategory.objects.all()
    result = []

    for obj in categories_obj:
        subcat=[]
        subcat_objects=subcategory_obj.filter(category=obj)
        for sub_obj in subcat_objects:
            subcat.append(sub_obj.subcategory)
        result.append({obj.name:subcat})
    return Response({'status':'success','data':result})


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def register_user(request):
    """
    API endpoint that registers users
    """
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        user = serializer.save()
        if user:
            token = Token.objects.create(user=user)
            json = serializer.data
            json['token'] = token.key
            return Response(json, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
#access with seller token
def add_catalogue(request):
    """
    API endpoint that add catalogue with list of items mapped under a category and sub category
    """
    #print (type(request.data))
    data=request.data

    #print(data)
    formatted_item_list = []

    #constructing json for serialzing to verify inputs
    for category in data:
        #print (category)
        category_name=category
        category_list=data[category]
        #print (category_list)
        for subcategory_list in category_list:
            #print(subcategory_list)
            for subcategory in subcategory_list:
                #print(subcategory)
                for item in subcategory_list[subcategory]:
                    item_json = {}
                    item_json['name'] = item.get('name','')
                    item_json['price'] = item.get('price','')
                    item_json['quantity'] = item.get('quantity','')
                    item_json['desc'] = item.get('desc','')
                    item_json['category']=category_name
                    item_json['subcategory']=subcategory
                    item_json['seller'] = request.user.id
                    formatted_item_list.append(item_json)
    serializer_list=[]
    serializer_error={}

    #validating the formatted json
    for item in formatted_item_list:
        serializer = CatalogueSerializer(data=item)
        #if invalid item entry updating the error json
        if not serializer.is_valid():
            serializer_error.update(serializer.errors)
        else:
        #if valid entry adding serialized objects
            serializer_list.append(serializer)

    #print (serializer_error)

    #To make adding catalogue API atomic the serialized objects are added in the database only when no error is encountered in given request
    if serializer_error == {}:
        for serializer in serializer_list:
            serializer.save()
        return Response("Catalogue Added Successfuly", status=status.HTTP_200_OK)
    else:
        return Response(serializer_error, status=status.HTTP_400_BAD_REQUEST)