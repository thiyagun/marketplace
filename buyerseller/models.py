from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=20)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'category'

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, db_column='name')
    subcategory = models.CharField(max_length=20)

    class Meta:
        db_table = 'sub_category'

    def __str__(self):
        return self.subcategory

#This model contains a list of available items
class Item(models.Model):
    name=models.CharField(max_length=30, default='')
    description = models.TextField(blank=True, null=True,default='')
    subcategory = models.ForeignKey(SubCategory)

    def __str__(self):
        return self.name

#This model consist of list of sellers offering the available above item at particular price and at  particular quantity
class SellersItem(models.Model):
    seller=models.ForeignKey(User)
    price=models.FloatField()
    qty=models.IntegerField(default=1)
    item = models.ForeignKey(Item)