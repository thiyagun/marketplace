# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-03 10:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('buyerseller', '0003_auto_20171203_1612'),
    ]

    operations = [
        migrations.AddField(
            model_name='sellersitem',
            name='item',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='buyerseller.Item'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='item',
            name='name',
            field=models.CharField(default='', max_length=30),
        ),
    ]
