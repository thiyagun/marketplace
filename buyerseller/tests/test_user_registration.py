from django.test import TestCase
from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token

class UserRegisterTestCase(APITestCase):
    url = '/user/register/'
    admin_token=''
    def setUp(self):
        self.username = "admin"
        self.password = "adminadmin"
        self.email = "admin@admin.com"
        self.user = User.objects.create_user(username=self.username, password=self.password, email=self.email)
        self.auth_token = Token.objects.create(user=self.user)
        self.admin_token=self.auth_token.key

    def successful_user_registration(self):
        data = {
            'username': 'testuserthiyagu',
            'email': 'thiyagu@admin.com',
            'password': 'testuserthiyagu'
        }

        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token))
        user = User.objects.latest('id')
        token = Token.objects.get(user=user)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], data['username'])
        self.assertEqual(response.data['email'], data['email'])
        self.assertEqual(response.data['token'], token.key)

    def test_create_user_with_preexisting_email(self):
        data = {
            'username': 'testuser2',
            'email': 'admin@admin.com',
            'password': 'testuser'
        }

        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)

    def test_create_user_with_invalid_email(self):
        data = {
            'username': 'test',
            'email':  'testing',
            'passsword': 'testing123'
        }


        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)

    def test_create_user_with_no_email(self):
        data = {
                'username' : 'test1233',
                'email': '',
                'password': 'asdasdasdas'
        }

        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token))
        print (response)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)

    def test_create_user_with_short_password(self):
        data = {
            'username': 'test',
            'email': 'asds@fs.com',
            'password': 'test'
        }

        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_no_password(self):
        data = {
            'username': 'test',
            'email': 'test@g.com',
            'password': ''
        }

        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_preexisting_username(self):
        data = {
            'username': 'admin',
            'email': 'user@test.com',
            'password': 'testuser'
        }

        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)
