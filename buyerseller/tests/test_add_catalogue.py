from django.test import TestCase
from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from buyerseller.models import Item,SellersItem, Category, SubCategory

class UserRegisterTestCase(APITestCase):
    url = '/catalogue/add/'
    admin_token=''
    def setUp(self):
        #Seller1 user registration
        self.seller1_username = "seller1"
        self.seller1_password = "seller1seller1"
        self.seller1_email = "seller1@seller1.com"
        self.user1 = User.objects.create_user(username=self.seller1_username, password=self.seller1_password, email=self.seller1_email)
        self.auth_token1 = Token.objects.create(user=self.user1)
        self.seller_token1=self.auth_token1.key

        self.seller2_username = "seller2"
        self.seller2_password = "seller2seller2"
        self.seller2_email = "seller2@seller2.com"
        self.user2 = User.objects.create_user(username=self.seller2_username, password=self.seller2_password,
                                              email=self.seller2_email)
        self.auth_token2 = Token.objects.create(user=self.user2)
        self.seller_token2 = self.auth_token2.key

        #Creating Categories
        self.cat_obj = Category.objects.create(name='games')
        self.cat_obj2 = Category.objects.create(name='shoes')

        #Creating Sub Category Object
        self.subcat_obj = SubCategory.objects.create(category=self.cat_obj, subcategory='ps4')
        self.subcat_obj2 = SubCategory.objects.create(category=self.cat_obj, subcategory='xbox')
        self.subcat_obj3= SubCategory.objects.create(category=self.cat_obj2, subcategory='adidas')





    def test_seller1_add_catalogue_successfully(self):
        data = {
			"games": [{
					"ps4": [{
							"name": "forza",
							"quantity": 1,
							"price": 2.50,
							"desc": "car race"
						}
					]
				},

				{
					"xbox": [{
							"name": "zombie",
							"quantity": 4,
							"price": 12.50,
							"desc": "arcade game"
						},

						{
							"name": "zombie2",
							"quantity": 4,
							"price": 12.50,
							"desc": "arcade game"
						}
					]
				}
			]
		,

			"shoes": [{
					"adidas": [{
							"name": "slipperrc",
							"quantity": 1,
							"price": 2.50,
							"desc": "good"
						}
					]
				}]
            }
        response = self.client.post(self.url, data, format='json', HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token1))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Item.objects.count(), 4)
        self.assertEqual(SellersItem.objects.count(),4)

    def test_seller2_add_same_catalogue_successfully(self):
        self.test_seller1_add_catalogue_successfully()
        data = {
            "games": [{
                "ps4": [{
                    "name": "forza",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "car race"
                }
                ]
            },

                {
                    "xbox": [{
                        "name": "zombie",
                        "quantity": 4,
                        "price": 12.50,
                        "desc": "arcade game"
                    },

                        {
                            "name": "zombie2",
                            "quantity": 4,
                            "price": 12.50,
                            "desc": "arcade game"
                        }
                    ]
                }
            ]
            ,

            "shoes": [{
                "adidas": [{
                    "name": "slipperrc",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "good"
                }
                ]
            }]
        }
        response = self.client.post(self.url, data, format='json',
                                    HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token2))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Item.objects.count(), 4)
        self.assertEqual(SellersItem.objects.count(), 8)

    def test_seller2_add_different_catalogue_successfully(self):
        self.test_seller1_add_catalogue_successfully()
        data = {
            "games": [{
                "ps4": [{
                    "name": "forza3",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "car race"
                }
                ]
            },

                {
                    "xbox": [{
                        "name": "zombie3",
                        "quantity": 4,
                        "price": 12.50,
                        "desc": "arcade game"
                    },

                        {
                            "name": "zombie4",
                            "quantity": 4,
                            "price": 12.50,
                            "desc": "arcade game"
                        }
                    ]
                }
            ]
            ,

            "shoes": [{
                "adidas": [{
                    "name": "slipperrc1",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "good"
                }
                ]
            }]
        }
        response = self.client.post(self.url, data, format='json',
                                    HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token2))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Item.objects.count(), 8)
        self.assertEqual(SellersItem.objects.count(), 8)


    def test_seller1_add_catalogue_with_already_added_items(self):
        self.test_seller1_add_catalogue_successfully()
        data = {
            "games": [{
                "ps4": [{
                    "name": "forza",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "car race"
                }
                ]
            },

                {
                    "xbox": [{
                        "name": "zombie",
                        "quantity": 4,
                        "price": 12.50,
                        "desc": "arcade game"
                    },

                        {
                            "name": "zombie2",
                            "quantity": 4,
                            "price": 12.50,
                            "desc": "arcade game"
                        }
                    ]
                }
            ]
            ,

            "shoes": [{
                "adidas": [{
                    "name": "slipperrc",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "good"
                }
                ]
            }]
        }
        response = self.client.post(self.url, data, format='json',
                                    HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token1))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Item.objects.count(), 4)
        self.assertEqual(SellersItem.objects.count(), 4)

    def test_seller1_add_disabled_category(self):
        self.cat_obj2 = Category.objects.filter(name='shoes').update(is_active=False)
        data = {
            "games": [{
                "ps4": [{
                    "name": "forza",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "car race"
                }
                ]
            },

                {
                    "xbox": [{
                        "name": "zombie",
                        "quantity": 4,
                        "price": 12.50,
                        "desc": "arcade game"
                    },

                        {
                            "name": "zombie2",
                            "quantity": 4,
                            "price": 12.50,
                            "desc": "arcade game"
                        }
                    ]
                }
            ]
            ,

            "shoes": [{
                "adidas": [{
                    "name": "slipperrc",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "good"
                }
                ]
            }]
        }
        response = self.client.post(self.url, data, format='json',
                                    HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token1))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Item.objects.count(), 0)
        self.assertEqual(SellersItem.objects.count(), 0)


    def test_seller1_add_catalogue_with_invalid_category_and_subcategory(self):
        self.cat_obj2 = Category.objects.filter(name='shoes').update(is_active=False)
        data = {
            "gamadses": [{
                "ps4": [{
                    "name": "forza",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "car race"
                }
                ]
            },

                {
                    "xbasdox": [{
                        "name": "zombie",
                        "quantity": 4,
                        "price": 12.50,
                        "desc": "arcade game"
                    },

                        {
                            "name": "zombie2",
                            "quantity": 4,
                            "price": 12.50,
                            "desc": "arcade game"
                        }
                    ]
                }
            ]
            ,

            "shoes": [{
                "adidasdas": [{
                    "name": "slipperrc",
                    "quantity": 1,
                    "price": 2.50,
                    "desc": "good"
                }
                ]
            }]
        }
        response = self.client.post(self.url, data, format='json',
                                    HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token1))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Item.objects.count(), 0)
        self.assertEqual(SellersItem.objects.count(), 0)



    def test_seller1_add_catalogue_with_invalid_input_attributes(self):
        self.cat_obj2 = Category.objects.filter(name='shoes').update(is_active=False)
        data = {
            "games": [{
                "ps4": [{
                    "name": "forza",
                    "desc": "car race"
                }
                ]
            },

                {
                    "xbox": [{
                        "name": "zombie",
                        "quantity": 4,
                        "price": 12.50,
                        "desc": "arcade game"
                    },

                        {
                            "name": "",
                            "quantity": 4,
                            "price": 12.50,
                            "desc": "arcade game"
                        }
                    ]
                }
            ]
            ,

            "shoes": [{
                "adidas": [{
                    "name": "slipperrc",
                    "quantity": 0,
                    "price": 2.50,
                    "desc": "good"
                }
                ]
            }]
        }
        response = self.client.post(self.url, data, format='json',
                                    HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token1))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Item.objects.count(), 0)
        self.assertEqual(SellersItem.objects.count(), 0)



