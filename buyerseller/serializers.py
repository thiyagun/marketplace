from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User
from buyerseller.models import SellersItem, SubCategory, Item, Category

class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    username = serializers.CharField(required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(min_length=8)

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'],
             validated_data['password'])
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')


class CatalogueSerializer(serializers.Serializer):
    price = serializers.FloatField(required=True)
    quantity = serializers.IntegerField(required=True,min_value=1)
    desc = serializers.CharField()
    name=serializers.CharField(required=True)
    category = serializers.CharField(required=True)
    subcategory = serializers.CharField(required=True)
    seller=serializers.IntegerField(required=True)

    def create(self, validated_data):

        try:
            item_obj = Item.objects.get(name= validated_data['name'],subcategory = validated_data['subcategory'])
        except:
            #add items only if there is no entry of the item
            item_obj =Item()
            item_obj.name=validated_data['name']
            item_obj.description=validated_data.get('desc','')
            item_obj.subcategory=validated_data['subcategory']
            item_obj.save()

        seller_obj= SellersItem()
        seller_obj.seller = User.objects.get(id=validated_data['seller'])
        seller_obj.qty= validated_data['quantity']
        seller_obj.price = validated_data['price']
        seller_obj.item = item_obj
        seller_obj.save()
        return seller_obj

    def validate_name(self,value):
       seller_id =  (self.initial_data['seller'])
       try:
           item_obj = SellersItem.objects.get(item__name=value,seller__id=seller_id)
       except:
           return value
       raise serializers.ValidationError("item already added by this seller")


    def validate_subcategory(self,value):
        category= self.initial_data['category']
        try:
            subcategory_obj=SubCategory.objects.get(subcategory=value, category__name = category)
        except:
            raise serializers.ValidationError("{} ==> {} is invalid".format(category, value))
        return subcategory_obj

    def validate_category(self, value):
        try:
            category_obj= Category.objects.get(name=value, is_active=True)
        except:
            raise serializers.ValidationError("cataegory {}  is invalid or disabled".format(value))
        return category_obj

