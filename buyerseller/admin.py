from django.contrib import admin
from buyerseller.models import Category,SubCategory,Item,SellersItem
# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name','is_active']

class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ['category','subcategory']

class ItemsAdmin(admin.ModelAdmin):
    list_display = ['name','subcategory','description']

class SellersItemAdmin(admin.ModelAdmin):
    list_display = ['seller','item','price','qty']

admin.site.register(Item, ItemsAdmin)
admin.site.register(SellersItem, SellersItemAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
