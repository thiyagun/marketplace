This repository contains Market place app which contains the following features
Features
·  User can be same as buyer and seller
·  User can add his catalog for selling
·  Catalog will have list of items 
·  Items would lie under a category and subcategory
·  Enable/disable category 
Framework  used: Django/Python
Database used: postgres
Testcase for all modules included 

=====================================
User registration Module:
The user can be registered as both buyer and seller
The API for this module is


POST /user/register/ HTTP/1.1
Host: 127.0.0.1:8000
Content-Type: application/json
Authorization: Token 364568e6a90b645acb9d18891c09d5e029841a33
Cache-Control: no-cache
Postman-Token: 52c6965f-ba94-8034-5376-322ae43bd623

{"username":"thiyagu2561",
"password":"werewerertyer",
"email": "t.nat@gmail.com"}

====================================

Category Enable/Disable Module:
The category can be enabled or disabled in Category table of django admin using is_active flag
The active categories and it's sub categories can be retrieved using the below API

GET /categories/ HTTP/1.1
Host: 127.0.0.1:8000
Authorization: Token 364568e6a90b645acb9d18891c09d5e029841a33
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: 11b36d21-399f-937e-96bf-6bd254ead62a

=======================================
Adding Catalogue Module:
The catalogue contains list of items grouped under a category and subcategory
Any seller can add their catalogue using the below API

POST /catalogue/add/ HTTP/1.1
Host: 127.0.0.1:8000
Authorization: Token 364568e6a90b645acb9d18891c09d5e029841a33
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: 6a790c92-95bb-4a9e-3188-ac0ae39a3f58

{ 
			"games": [{
					"ps4": [{
							"name": "forza",
							"quantity": 1,
							"price": 2.50,
							"desc": "car race"
						}
					]
				},

				{
					"xbox": [{
							"name": "zombie",
							"quantity": 4,
							"price": 12.50,
							"desc": "arcade game"
						},

						{
							"name": "zombie2",
							"quantity": 4,
							"price": 12.50,
							"desc": "arcade game"
						}
					]
				}
			]
		,
		
			"shoes": [{
					"adidas": [{
							"name": "slipperrc",
							"quantity": 1,
							"price": 2.50,
							"desc": "good"
						},

						{
							"name": "sneaker",
							"quantity": 3,
							"price": 3.50,
							"desc": "better"
						}
					]
				}]
}



Note: A same item can be added by multiple sellers but there will be only single entry for an item with multiple seller entries with their seller's price and quantity.
		
	







